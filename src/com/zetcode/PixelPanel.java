package com.zetcode;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputListener;

import org.w3c.dom.events.MouseEvent;

class PixelPanel extends JPanel implements MouseMotionListener, MouseInputListener, MouseWheelListener, Component {
	

	int displayWidth = ImageClass.width;
	int displayHeight = ImageClass.height;
	
	
	//Offset calc
	int offsetX = 0;
	int offsetY = 0;
	int dragStartX;
	int dragStartY;
	boolean dragging = false;
	
	int selectionDragStartX;
	int selectionDragStartY;
	int selectionDragWidth;
	int selectionDragHeight;
	boolean selectionDragging = false;
		
	int zoom = 1;
	
	private List<requestRepaintListener> listeners = new ArrayList<requestRepaintListener>();
	
	boolean leftMouseBtnState = false;

    public PixelPanel() {
        setBorder(BorderFactory.createLineBorder(Color.black));
    	addMouseListener(this); 
    	addMouseWheelListener(this);
    	addMouseMotionListener(this);

        ImageClass.createImageArray();
        ImageClass.createImage(); 
        
        System.out.println("dit is eenmalig");
    }

    public Dimension getPreferredSize() {
        return new Dimension(700,700);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);       

        g.drawImage(ImageClass.img,
        		offsetX, offsetY, displayWidth * zoom,displayHeight * zoom, null);
        
        g.setColor(new Color(0,0,255,50));
        
        Rectangle r = new Rectangle(selectionDragStartX,selectionDragStartY,selectionDragWidth,selectionDragHeight);
        g.fillRect(selectionDragStartX,selectionDragStartY,selectionDragWidth,selectionDragHeight); 
        
        notifyListeners();
    }

	@Override
	public void mouseClicked(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		if(ToolBar.activeTool == "fill") {
			if (SwingUtilities.isLeftMouseButton(e)) {
				
				System.out.println(getPixelX(e.getX()) + " " + getPixelY(e.getY()));
				
				int oldColor = ImageClass.img.getRGB(getPixelX(e.getX()), getPixelY(e.getY()));
				fill(getPixelX(e.getX()), getPixelY(e.getY()), oldColor);
				this.repaint();
			}
			
			
		}	
	}

	@Override
	public void mouseEntered(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println(e.getButton());
		
		if(ToolBar.activeTool == "pen") {
			if (SwingUtilities.isLeftMouseButton(e)) {
				ImageClass.img.setRGB(getPixelX(e.getX()), getPixelY(e.getY()), PalletPanel.colorArray[PalletPanel.activePallete].getRGB());
				this.repaint();
			}
		}
	}

	@Override
	public void mouseReleased(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		if (SwingUtilities.isMiddleMouseButton(e)) {
			dragging = false;
		}
		
		if(ToolBar.activeTool == "fill") {
			if (SwingUtilities.isLeftMouseButton(e)) {
				
				int oldColor = ImageClass.img.getRGB(getPixelX(e.getX()), getPixelY(e.getY()));
				fill(getPixelX(e.getX()), getPixelY(e.getY()), oldColor);
			}
		}	
		
	}

	@Override
	public void mouseDragged(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
			
		
		if(ToolBar.activeTool == "pen") {
			if (SwingUtilities.isLeftMouseButton(e)) {
				ImageClass.img.setRGB(getPixelX(e.getX()), getPixelY(e.getY()), PalletPanel.colorArray[PalletPanel.activePallete].getRGB());
			}
		}
		
		if(ToolBar.activeTool == "selection") {
			if (SwingUtilities.isLeftMouseButton(e)) {
				if(selectionDragging == false) {
					selectionDragStartX = e.getX();
					selectionDragStartY = e.getY();
					selectionDragging = true;
				}else {
					selectionDragWidth = e.getX() - selectionDragStartX;
					selectionDragHeight = e.getY() - selectionDragStartY;
				}
			}
		}
		
		if (SwingUtilities.isMiddleMouseButton(e)) {
			System.out.println(e);
			
			if(dragging == false) {
				dragStartX = e.getX();
				dragStartY = e.getY();
				dragging = true;
			}else {
				offsetX -= dragStartX - e.getX();
				dragStartX = e.getX();
				
				offsetY -= dragStartY - e.getY();
				dragStartY = e.getY();
			}	
		}
		this.repaint();
	}

	@Override
	public void mouseMoved(java.awt.event.MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// TODO Auto-generated method stub
		System.out.println(e.getWheelRotation());
		System.out.println(e.getScrollAmount());
		
		if(e.getWheelRotation() == -1) {
			zoom ++;
		}else {
			if(zoom != 1) {
				zoom --;
			}
		}
		
		repaint();
	}

	@Override
	public void update(Object o) {
		// TODO Auto-generated method stub
		
	}
	
	public int getPixelX(int mouseX) {
		return (mouseX - offsetX) / zoom;
		
	}
	
	public int getPixelY(int mouseY) {
		return (mouseY - offsetY) / zoom;
	}
	
	public void fill(int x, int y, int oldColor) {
		
//		System.out.println("Orginele kleur: " + oldColor);
//		System.out.println("x Y plek: " + x + " " + y);
//		System.out.println("x Y kleur: " + ImageClass.img.getRGB(x, y));
		
				
		if(x < ImageClass.width && x >= 0 && y < ImageClass.height && y >= 0) {
			
			if(ImageClass.img.getRGB(x, y) == PalletPanel.colorArray[PalletPanel.activePallete].getRGB()) { return;}
			
			if(ImageClass.img.getRGB(x, y) != oldColor) {return;}
			
//			System.out.println("hij tekent het hokje in");
			ImageClass.img.setRGB(x, y, PalletPanel.colorArray[PalletPanel.activePallete].getRGB());
			
			fill(x - 1, y, oldColor);
			fill(x + 1, y, oldColor);
			fill(x, y - 1, oldColor);
			fill(x, y + 1, oldColor);
		}
		
	}
	
	 public void addListener(requestRepaintListener listener) {
        listeners.add(listener);
    }
    


    private void notifyListeners() {
        for (requestRepaintListener listener : listeners) {
            listener.onSubmitted("blablabla");
        }
    }
}
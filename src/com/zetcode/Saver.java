package com.zetcode;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.JPanel;

public class Saver {
	
	public static void save() throws IOException
	{
		//Makes sure the current image is saved in the array
		ImageClass.bufferedImageToArray();
	     
	    
	    BufferedWriter outputWriter = null;
	    outputWriter = new BufferedWriter(new FileWriter("c:/temp/samplefile1.txt"));
	    for (int i = 0; i <ImageClass.imgArray.length; i++) {
	    	for(int j = 0; j < ImageClass.imgArray[i].length; j++){

		      outputWriter.write(Integer.toString(ImageClass.imgArray[i][j]));
		      
		      if(j < ImageClass.imgArray[i].length - 1) {
		 	    outputWriter.write(",");
		      }
		      
		      
		      
		    }
	    	outputWriter.newLine();
	    }
	    outputWriter.flush();  
	    outputWriter.close();  
	     
	}
	
	public static void open(JPanel pixelPanel)  throws IOException {

        // This will reference one line at a time
        String line = null;
        // FileReader reads text files in the default encoding.
        FileReader fileReader = 
            new FileReader("c:/temp/samplefile1.txt");

        // Always wrap FileReader in BufferedReader.
        BufferedReader bufferedReader = 
            new BufferedReader(fileReader);

        int i = 0;
        while((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
            
            String[] lineColors = line.split(",");
            
            for(int j =0; j <lineColors.length; j++) {
            	ImageClass.imgArray[i][j] = Integer.parseInt(lineColors[j]);
            }
            
            i++;
        }  
        ImageClass.createImage();
        
        pixelPanel.repaint();
	}



}

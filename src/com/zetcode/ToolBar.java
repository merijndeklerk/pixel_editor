package com.zetcode;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JToolBar;

public class ToolBar extends JToolBar {
	
	static String activeTool;
	static JButton penButton = new JButton();
	static JButton fillButton = new JButton();
	JButton selectButton = new JButton();
	
	static Icon penIcon =new ImageIcon("res/penicon.png");
	static Icon penPressedIcon =new ImageIcon("res/peniconPressed.png");
	
	static Icon fillIcon =new ImageIcon("res/fillicon.png");
	Icon fillPressedIcon =new ImageIcon("res/filliconPressed.png");
	
	
	Icon selectIcon =new ImageIcon("res/selection.png");
	Icon selectPressedIcon =new ImageIcon("res/selection.png");
	
	public void changeActiveTool(String tool) {
		
	}
	

   //function that handles the unpressing of the menu bar buttons
   public static void unPressButtons() {
	   penButton.setIcon(penIcon);
	   fillButton.setIcon(fillIcon);
   }
	   
	
	public ToolBar() {
        //Toolbar test

	    setRollover(true);
	    this.setBorder(BorderFactory.createLineBorder(null));
	    	    
	    penButton.setPreferredSize(new Dimension(32,32));
	    penButton.setIcon(penIcon);
	    penButton.setBorder(null);
	    penButton.setBackground(new Color(55,55,55));
	    penButton.addActionListener(new ActionListener()
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	unPressButtons();
	        	activeTool = "pen";
	    	   
	    	    penButton.setIcon(penPressedIcon);

	        	
	        }
	      });
	    add(penButton);

	    fillButton.setPreferredSize(new Dimension(32,32));
	    fillButton.setIcon(fillIcon);
	    fillButton.setBorder(null);
	    fillButton.setBackground(new Color(55,55,55));
	    fillButton.addActionListener(new ActionListener()
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	unPressButtons();
	        	activeTool = "fill";
	    	   
	    	    fillButton.setIcon(fillPressedIcon);

	        	
	        }
	      });
	    add(fillButton);
	    
	    JButton selectionButton = new JButton();
	    selectionButton.setPreferredSize(new Dimension(32,32));
	    selectionButton.setIcon(selectIcon);
	    selectionButton.setBorder(null);   
	    selectionButton.setBackground(new Color(55,55,55));
	    selectionButton.addActionListener(new ActionListener()
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	activeTool = "selection";
	        }
	      });
	    add(selectionButton);
	    addSeparator();
	    	    

	}
	
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(new Color(55,55,55));
        
        g2d.fillRect(0, 0, getWidth() - 1, getHeight() - 1);

    }

}

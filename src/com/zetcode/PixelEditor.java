package com.zetcode;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

public class PixelEditor {
	
   public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI(); 
            }
        });
    }
   

    private static void createAndShowGUI() {

        JFrame frame = new JFrame("Merijn's pixel editor");
        frame.setSize(1200,800);
        
        JSplitPane split = new JSplitPane();
        
        PixelPanel pixelPanel = new PixelPanel();
        pixelPanel.setBackground(new Color(43,43,43));
        
        split.setUI(new BasicSplitPaneUI() {
            public BasicSplitPaneDivider createDefaultDivider() {
            return new BasicSplitPaneDivider(this) {
                public void setBorder(Border b) {
                }

                @Override
                    public void paint(Graphics g) {
                    g.setColor(new Color(55,55,55));
                    g.fillRect(0, 0, getSize().width, getSize().height);
                        super.paint(g);
                    }
            };
            }
        });
        
        split.setBorder(null);
        split.setDividerLocation(900);
        
        JPanel rightPanel = new JPanel();
        rightPanel.setBackground(new Color(43,43,43));
        
        HSBSelector hSBSelector = new HSBSelector();
        hSBSelector.setBackground(new Color(43,43,43));
        rightPanel.add(hSBSelector);
        
        
        PalletPanel palletPanel = new PalletPanel(hSBSelector);
        palletPanel.setBackground(new Color(43,43,43));
        rightPanel.add(palletPanel);
        
        Viewer viewer = new Viewer();
        viewer.setBackground(new Color(43,43,43));
        rightPanel.add(viewer);
        
        hSBSelector.addListener(palletPanel);
        pixelPanel.addListener(viewer);
     
        split.setLeftComponent(pixelPanel);
        split.setRightComponent(rightPanel);
           
	    JMenuBar menuBar = new MenuBar(pixelPanel);

	    // put the menubar on the frame
	    frame.setJMenuBar(menuBar);
        
	    
	    frame.add(new ToolBar(), BorderLayout.PAGE_START);	
        frame.add(split);	

        frame.setVisible(true);	

    }
}

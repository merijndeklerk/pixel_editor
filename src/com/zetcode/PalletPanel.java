package com.zetcode;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LinearGradientPaint;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

import org.w3c.dom.css.RGBColor;

class PalletPanel extends JPanel implements MouseInputListener, MouseMotionListener, requestRepaintListener {
	
	static Color[] colorArray = new Color[5];
	
	JPanel HSBPanel;
	
	static int activePallete = 3;

    public PalletPanel(JPanel HSBPanelObj) {
    	    	
    	HSBPanel = HSBPanelObj;
    	
    	colorArray[0] = Color.gray;
    	colorArray[1] = Color.gray;
    	colorArray[2] = Color.gray;
    	colorArray[3] = Color.gray;
    	colorArray[4] = Color.gray;
        addMouseListener(this);
        addMouseMotionListener(this);
    }


    public Dimension getPreferredSize() {
        return new Dimension(250,30);
    }


    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        for(int i = 0; i < colorArray.length; i++) {
            g.setColor(colorArray[i]);
            g.fillRect(i * 25, 1, 20, 20);
            
            if(i == activePallete) {
                g.setColor(Color.red);
                g.drawRect(i * 25, 1, 20, 20);
            }
        }
    }

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub		
		int mouseX = e.getX();
		int mouseY = e.getY();
		
		System.out.println(mouseX);
		
		activePallete = mouseX / 25;
		
		float[] hsb = Color.RGBtoHSB(colorArray[activePallete].getRed(), colorArray[activePallete].getGreen(), colorArray[activePallete].getBlue(), null);

		
		System.out.println(hsb[0] * 100);
		HSBSelector.activeSaturation = Math.round(hsb[1] * 100);
		HSBSelector.activeHue = Math.round(hsb[0] * 100);
		HSBSelector.activeBrightness = Math.round(hsb[2] * 100);
		
		HSBPanel.repaint();
		
		
		
//		if (( mouseX >= hueSelector.getMinX() && mouseX <= hueSelector.getMaxX() )   // check if X is within range
//			   && ( mouseY >= hueSelector.getMinY() && mouseY <= hueSelector.getMaxY())) {
//			PalletPanel.activeColor = Color.HSBtoRGB((float) (mouseY / 200.0), 1f, 1f);
//		}
		this.repaint();
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onSubmitted(String value) {
		// TODO Auto-generated method stub

		this.repaint();
		
	}  
}
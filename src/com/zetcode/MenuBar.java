package com.zetcode;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Menu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class MenuBar extends JMenuBar implements ActionListener {
	
    Color bgColor= Color.white;

    public void setColor(Color color) {
        bgColor=color;
    }

	public MenuBar(JPanel pixelPanel) {
		 Font f = new Font("sans-serif", Font.PLAIN, 12);
         UIManager.put("Menu.font", f);
         UIManager.put("MenuItem.font", f);
        


		this.setBorder(BorderFactory.createLineBorder(null));
	    //menu test
	        
	    // build the File menu
	    JMenu fileMenu = new JMenu("fla");
	     
    
	    JMenuItem openMenuItem = new JMenuItem("Open");
	    fileMenu.add(openMenuItem);
	    
	    openMenuItem.addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent actionEvent) {
	        	System.out.println("clicke");
	        	try {
					Saver.open(pixelPanel);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	    });
	    
	    JMenuItem saveMenuItem = new JMenuItem("save");
	    fileMenu.add(saveMenuItem);
	    
	    saveMenuItem.addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent actionEvent) {
	        	System.out.println("clicke");
	        	try {
					Saver.save();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	    });
	    

	    // build the Edit menu
	    JMenu editMenu = new JMenu("Edit");
	    JMenuItem cutMenuItem = new JMenuItem("Cut");
	    JMenuItem copyMenuItem = new JMenuItem("Copy");
	    JMenuItem pasteMenuItem = new JMenuItem("Paste");
	    editMenu.add(cutMenuItem);
	    editMenu.add(copyMenuItem);
	    editMenu.add(pasteMenuItem);

	    // add menus to menubar
	    add(fileMenu);
	    add(editMenu);
	}
	
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(bgColor);
        
        g2d.fillRect(0, 0, getWidth() - 1, getHeight() - 1);

    }

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
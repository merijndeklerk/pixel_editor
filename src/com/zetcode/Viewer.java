package com.zetcode;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputListener;

public class Viewer extends JPanel implements MouseMotionListener, MouseInputListener, MouseWheelListener, Component, requestRepaintListener {

	//Offset calc
	int offsetX = 0;
	int offsetY = 0;
	int dragStartX;
	int dragStartY;
	boolean dragging = false;
	
	int selectionDragStartX;
	int selectionDragStartY;
	int selectionDragWidth;
	int selectionDragHeight;
	boolean selectionDragging = false;
	
	int displayWidth = ImageClass.width;
	int displayHeight = ImageClass.height;
		
	int zoom = 1;
	
	boolean leftMouseBtnState = false;

	public Viewer() {
    	

        setBorder(BorderFactory.createLineBorder(Color.black));
    	addMouseListener(this); 
    	addMouseWheelListener(this);
    	addMouseMotionListener(this);
    }
    
    
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawImage(ImageClass.img,
        		offsetX, offsetY, displayWidth * zoom,displayHeight * zoom, null);
            
        g.setColor(Color.RED);
        g.drawString("Zoom: x" + zoom, 5, 14);
        
    }


    public Dimension getPreferredSize() {
        return new Dimension(250,200);
    }

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		if (SwingUtilities.isMiddleMouseButton(e)) {
			dragging = false;
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Object o) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// TODO Auto-generated method stub
		if(e.getWheelRotation() == -1) {
			zoom ++;
		}else {
			if(zoom != 1) {
				zoom --;
			}
		}
		
		System.out.println(e.getWheelRotation());
		System.out.println(e.getScrollAmount());
		
		repaint();
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		if (SwingUtilities.isMiddleMouseButton(e)) {
			System.out.println(e);
			
			if(dragging == false) {
				dragStartX = e.getX();
				dragStartY = e.getY();
				dragging = true;
			}else {
				offsetX -= dragStartX - e.getX();
				dragStartX = e.getX();
				
				offsetY -= dragStartY - e.getY();
				dragStartY = e.getY();
			}	
		}
		this.repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onSubmitted(String value) {
		// TODO Auto-generated method stub
		this.repaint();
	} 

}

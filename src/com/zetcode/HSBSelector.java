package com.zetcode;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LinearGradientPaint;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

import org.w3c.dom.css.RGBColor;

class HSBSelector extends JPanel implements MouseInputListener, MouseMotionListener {
	private final Color H0 = new Color( 255, 0, 0 );
    private final Color H1 = new Color( 255, 255, 0 );
    private final Color H2 = new Color( 0, 255, 0 );
    private final Color H3 = new Color( 0, 255, 255 );
    private final Color H4 = new Color( 0, 0, 255 );
    private final Color H5 = new Color( 255, 0, 255 );
    

    static int activeHue = 0;
    static int activeSaturation = 100;
    static int activeBrightness = 100;
    
    private List<requestRepaintListener> listeners = new ArrayList<requestRepaintListener>();

    Rectangle2D hueSelector = new Rectangle2D.Double( 220, 0, 25, 200 );
    Rectangle2D saturationBrightnessSelector = new Rectangle2D.Double( 0, 0, 200, 200 );
    
     
    private final Color[] BASE = { H0, H1, H2, H3, H4, H5, H0 };
     

    public HSBSelector() {
        addMouseListener(this);
        addMouseMotionListener(this);
    }
    

    public Dimension getPreferredSize() {
        return new Dimension(250,220);
    }


    public void paintComponent(Graphics g) {
        super.paintComponent(g);       
        
        Graphics2D g2d = (Graphics2D)g;
        int w = 200;
        int h = 200;

        // verticaal
        GradientPaint gp = new GradientPaint(
        		0, 0, Color.black, 
        		0, h, new Color(0,0,0,0)
                );

        // horizontaal
        GradientPaint gp2 = new GradientPaint(
                0, 0, Color.white,
                w, 0,new Color(Color.HSBtoRGB((float) (activeHue / 200.0), 1f, 1f)), true);

        g2d.setPaint(gp2);
        g2d.fill(saturationBrightnessSelector);
        g2d.setPaint(gp);
        g2d.fill(saturationBrightnessSelector);



      
        Point2D start = new Point2D.Float(0, 0);
        Point2D end = new Point2D.Float(0, 200);
        
        

        float[] pts = { 0f, 1f/6, 2f/6, 3f/6, 4f/6, 5f/6, 1f };
        LinearGradientPaint linGrad = new LinearGradientPaint( start, end, pts, BASE );
         
        g2d.setPaint( linGrad );
        g2d.fill(hueSelector);
        
        g2d.setColor(Color.black);
        g2d.fillRect (215, activeHue, 35, 4); 
        
        g2d.setColor(Color.black);
        g2d.fillRect (activeSaturation -2, activeBrightness -2, 5, 5); 
        


    }

	@Override
	public void mouseClicked(MouseEvent e) {
//		 TODO Auto-generated method stub		
//		int mouseX = e.getX();
//		int mouseY = e.getY();
//		
//		if (( mouseX >= hueSelector.getMinX() && mouseX <= hueSelector.getMaxX() )   // check if X is within range
//			   && ( mouseY >= hueSelector.getMinY() && mouseY <= hueSelector.getMaxY())) {
//			HSBSelector.activeColor = Color.HSBtoRGB((float) (mouseY / 200.0), 1f, 1f);
//		}
//		this.repaint();
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("hue: " + activeHue);
		System.out.println("bright: " + activeBrightness);
		System.out.println("sat: " + activeSaturation);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub

		
		int mouseX = e.getX();
		int mouseY = e.getY();
		
		//Hue selection
		if (( mouseX >= hueSelector.getMinX() && mouseX <= hueSelector.getMaxX() )   // check if X is within range
			   && ( mouseY >= hueSelector.getMinY() && mouseY <= hueSelector.getMaxY())) {
			

			//HSBSelector.activeColor = Color.HSBtoRGB((float) (mouseY / 200.0), (float) (activeSaturation / 200.0), (float) (activeBrightness/200.0));
			
			PalletPanel.colorArray[PalletPanel.activePallete] = new Color(Color.HSBtoRGB((float) (mouseY / 200.0), (float) (activeSaturation / 200.0), (float) (activeBrightness/200.0)));
			
			notifyListeners();
			
			this.activeHue = mouseY;
		}
		
		//Bright sat selector
		if (( mouseX >= saturationBrightnessSelector.getMinX() && mouseX <= saturationBrightnessSelector.getMaxX() )   // check if X is within range
				   && ( mouseY >= saturationBrightnessSelector.getMinY() && mouseY <= saturationBrightnessSelector.getMaxY())) {
				//HSBSelector.activeColor = Color.HSBtoRGB((float) (activeHue / 200.0), (float) (mouseX / 200.0), (float) (mouseY / 200.0));

				PalletPanel.colorArray[PalletPanel.activePallete] = new Color(Color.HSBtoRGB((float) (activeHue / 200.0), (float) (mouseX / 200.0), (float) (mouseY / 200.0)));
				
				notifyListeners();
				
				
				this.activeSaturation = mouseX;
				this.activeBrightness = mouseY;
		}
		
		this.repaint();
		
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	

    public void addListener(requestRepaintListener listener) {
        listeners.add(listener);
    }
    


    private void notifyListeners() {
        for (requestRepaintListener listener : listeners) {
            listener.onSubmitted("blablabla");
        }
    }
}
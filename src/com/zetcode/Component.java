package com.zetcode;

public interface Component {
    public void update(Object o);
}
package com.zetcode;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class ImageClass {

	  //image dimension
    static int width = 32;
    static int height = 32		;
    static BufferedImage img;
    static int[][] imgArray = new int[width][height];
	
	static public void createImageArray() {
		
		

		System.out.println("dit is eenmalig");
        //create random image pixel by pixel
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                int a = (int)(255);   //alpha
                int r = (int)(255);   //red
                int g = (int)(255);   //green
                int b = (int)(255);   //blue

                int p = (a<<24) | (r<<16) | (g<<8) | b; //pixel
  
                imgArray[x][y] = p;
            }
        }   
        System.out.println("dit is eenmalig");
	}

	static void createImage() {
		
        //create buffered image object img
        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        
        //create random image pixel by pixel
        for(int y = 0; y < imgArray.length; y++){
            for(int x = 0; x < imgArray[y].length; x++){
                img.setRGB(x, y, imgArray[x][y]);
            }
        }           
	}
	
	static public void bufferedImageToArray(){
		
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
            	imgArray[x][y] = img.getRGB(x, y);	
            }
        }

	}

}

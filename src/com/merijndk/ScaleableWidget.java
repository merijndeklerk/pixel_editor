package com.merijndk;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import java.awt.event.MouseMotionListener;

import javax.swing.event.MouseInputListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.GridData;

public class ScaleableWidget extends Composite {

	protected Shell shell;
	protected Display display;
	public boolean draggingBool = false;
	int lastTickY;
	int height = 123;


	public ScaleableWidget(Composite composite, int arg1, Display display) {
		super(composite, arg1);
			
	    Listener listener = new Listener() {
	    	public void handleEvent(Event event) {
	    		switch (event.type) {
	    			case SWT.MouseDown:

		    				draggingBool = true;
		    				lastTickY = event.y;
	    				break;
	    			case SWT.MouseMove:
	    				
	    				if(draggingBool) {
	        		  
	    					int pixelsToMove = event.y - lastTickY;
	    					height +=pixelsToMove;
	    					
	    					GridData gd = new GridData();
	    					gd.heightHint = height;
	    					gd.grabExcessHorizontalSpace = true;
	    					gd.horizontalAlignment = SWT.FILL;
	    					setLayoutData(gd);

	    					lastTickY = event.y;
	    					composite.layout();
	    				}
	            
	    				break;
	    			case SWT.MouseUp:
	    				draggingBool = false;
	    				break;
	    		}
	    	}
	    };
		addListener(SWT.MouseDown, listener);
		addListener(SWT.MouseUp, listener);
		addListener(SWT.MouseMove, listener);

	}
}

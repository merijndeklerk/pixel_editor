package com.merijndk;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import swing2swt.layout.BoxLayout;
import org.eclipse.swt.custom.ViewForm;

public class PixelEditor {

	protected Shell shell;


	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			PixelEditor window = new PixelEditor();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents(display);
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents(Display display) {
		shell = new Shell();
		shell.setSize(1100, 727);
		shell.setText("SWT Application");
		shell.setLayout(new FormLayout());
			
		
		SideBarWidget composite = new SideBarWidget(shell, SWT.BORDER, display);
		composite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BORDER));
		composite.setLayout(new GridLayout(1, false));
		FormData fd_composite = new FormData();
		fd_composite.top = new FormAttachment(0);
		fd_composite.right = new FormAttachment(100);
		fd_composite.bottom = new FormAttachment(0, 688);
		fd_composite.left = new FormAttachment(100, -300);
		composite.setLayoutData(fd_composite);
		
		HSBSelector composite_1 = new HSBSelector(composite, SWT.BORDER, display);
		GridData gd = new GridData();
		gd.heightHint = 123;
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = SWT.FILL;
		composite_1.setLayoutData(gd);
		
		ScaleableWidget composite_2 = new ScaleableWidget(composite, SWT.BORDER, display);
		GridData gd_2 = new GridData();
		gd_2.heightHint = 123;
		gd_2.grabExcessHorizontalSpace = true;
		gd_2.horizontalAlignment = SWT.FILL;
		composite_2.setLayoutData(gd_2);
		
		

	}
}

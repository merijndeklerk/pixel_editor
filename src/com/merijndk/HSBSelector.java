package com.merijndk;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.zetcode.requestRepaintListener;

public class HSBSelector extends ScaleableWidget {
	
	private final Color H0 = new Color( 255, 0, 0 );
    private final Color H1 = new Color( 255, 255, 0 );
    private final Color H2 = new Color( 0, 255, 0 );
    private final Color H3 = new Color( 0, 255, 255 );
    private final Color H4 = new Color( 0, 0, 255 );
    private final Color H5 = new Color( 255, 0, 255 );
    

    static int activeHue = 0;
    static int activeSaturation = 100;
    static int activeBrightness = 100;
    
    private List<requestRepaintListener> listeners = new ArrayList<requestRepaintListener>();

    Rectangle2D hueSelector = new Rectangle2D.Double( 220, 0, 25, 200 );
    Rectangle2D saturationBrightnessSelector = new Rectangle2D.Double( 0, 0, 200, 200 );
    
     
    private final Color[] BASE = { H0, H1, H2, H3, H4, H5, H0 };

	public HSBSelector(Composite composite, int arg1, Display display) {
		super(composite, arg1, display);
		// TODO Auto-generated constructor stub
		
		
	}



}

package com.merijndk;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import java.awt.event.MouseMotionListener;

import javax.swing.event.MouseInputListener;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;

public class SideBarWidget extends Composite {

	protected Shell shell;
	protected Display display;
	public boolean draggingBool = false;
	int lastTickX;
	int width = 300;


	public SideBarWidget(Shell shell, int arg1, Display display) {
		super(shell, arg1);

		this.shell = shell;
		this.display = display;
		
		
	    Listener listener = new Listener() {
	    	public void handleEvent(Event event) {
	    		switch (event.type) {
	    			case SWT.MouseDown:
	    				if(event.x < 10) {
		    				draggingBool = true;
		    				lastTickX = event.x;
	    				}
	    				break;
	    			case SWT.MouseMove:
	    				
	    				if(event.x < 10) {
	    					Cursor cursor = new Cursor(display, SWT.CURSOR_HAND);
	    					setCursor(cursor);
	    				}else {
	    					Cursor cursor = new Cursor(display, SWT.CURSOR_ARROW);
	    					setCursor(cursor);
	    				}
	    				if(draggingBool) {
	        		  
	    					int pixelsToMove = event.x - lastTickX;
	    					width -=pixelsToMove;

	    					FormData fd_composite = new FormData();
	    					fd_composite.top = new FormAttachment(0);
	    					fd_composite.right = new FormAttachment(100);
	    					fd_composite.bottom = new FormAttachment(0, 688);
							fd_composite.left = new FormAttachment(100, -width);
	        		
							setLayoutData(fd_composite);
							
							shell.layout();

	    				}
	            
	    				break;
	    			case SWT.MouseUp:
	    				draggingBool = false;
	    				break;
	    		}
	    	}
	    };
		addListener(SWT.MouseDown, listener);
		addListener(SWT.MouseUp, listener);
		addListener(SWT.MouseMove, listener);
	}
}
